--Include io, os, and nixio
require "os"
require "io"
require "nixio"

local function main() 
  if arg[1] == "start" then
    posix.
    start()
  elseif arg[1] == "stop" then
    stop()
  elseif arg[1] == "reset" then
    reset()
  end
end
-- backup and save lockfiles to /etc/lockdown/lock on sigterm
local function sigterm()
  nixio.syslog("LOCKDOWN: SIGTERM caught backing up lock files.")
  if lock_backup() then
    nixio.syslog("LOCKDOWN: Backed up lock files Succeded.")
  else
    nixio.syslog("LOCKDOWN: Backup of lock files Failed.")
  end 
  os.exit(0) 
end

-- fuction to see if usbmuxd is started
local function usbmuxd_chk()
  local pid = io.open("/var/run/usbmuxd.pid")
  if pid == nil then
    return false
  else
    return true
   end
end

-- Daemonize the LOCKDOWN process.
-- @return boolean or nil, error code, error message
local function daemonize()
  if nixio.getppid() == 1 then
    return
  end
  
  local pid, code, msg = nixio.fork()
  if not pid then
    return nil, code, msg
  elseif pid > 0 then
    os.exit(0)
  end
  
  nixio.setsid()
  nixio.chdir("/")
  
  local devnull = nixio.open("/dev/null", nixio.open_flags("rdwr"))
  nixio.dup(devnull, nixio.stdin)
  nixio.dup(devnull, nixio.stdout)
  nixio.dup(devnull, nixio.stderr)
  
  return true
end
-- Restore lock filse from /etc/lockdown/lock
local function lock_restore()
  for each in nixio.fs.dir("/etc/lockdown/lock") do
    nixio.syslog("LOCKDOWN: Restoring " .. each)
    nixio.fs.copy("/etc/lockdown/lock"..each,"/var/lib/lockdown/"..each)
   end
end
--Backup lock files from /var/lib/lockdown
local function lock_backup()
  local count = 0
  for each in nixio.fs.dir("/var/lib/lockdown") do
    if not each == "SystemConfiguration.plist" then
      nixio.syslog("LOCKDOWN: Backing up " .. each )
      nixio.fs.copy("/var/lib/lockdown/".. each, "/etc/lockdown/lock/" .. each)
      count = coutnt +1
    end
  end
  if count > 0 then
    return true
  else
    return false
  end
end

local function periodic_backup() 
  nixio.nanosleep(30,0)
  lock_backup()
  return nil
end

local function start()
  --Run as a Daemon
  if not daemonize() then
    nixio.syslog("LOCKDOWN: Could not fork to Daemonize the procees EXITING.")
    os.exit(0)
  end 
  
  --set SIGTERM handler
  nixio.signal(nixio.constant.SIGTERM,sigterm())
  
  --set pid in /var/run/lockdown.pid
  local pid = nixio.getpid()
  nixio.fs.writefile("/var/run/lockdown.pid",pid)
  
  -- check to see if usbmuxd is started
  if usbmuxd_chk() then
    nixio.syslog("LOCKDOWN: USBMUXD is started starting process")
  else
    nixio.syslog("LOCKDOWN: USBMUXD faild to start exiting")
    os.exit(0)
  end
  
  --runing restore 
  nixio.syslog("LOCKDOWN: Preparing to restore lock files.")
  lock_restore()
  --backup files every 30 sec 
  repeat perodic_backup() until true
 end
 
local funciton stop()
  local pidfile = io.open("/var/run/lockdown.pid","r")
  if not pidfile then
    local pid = pidfile:read("*n")
    nixio.syslog("LOCKDOWN: Sending SIGTERM to PID: "..pid)
    nixio.kill(pid,nixio.constant.SIGTERM)
    nixio.syslog("LOCKDOWN: EXITING")
    os.exit(0)
  else
    nixio.syslog("LOCKDOWN: Process not started.")
    os.exit(0) 
end

local function reset()
  for each in nixio.fs.dir("/etc/lockdown/lock") do
    nixio.syslog("LOCKDOWN: RESET" .. each)
    nixio.fs.remove("/etc/lockdown/lock/"..each)
  end
  for each in nixio.fs.dir("/var/lib/lockdown/") do
    nixio.syslog("LOCKDOWN: RESET " .. each)
    nixio.fs.remove("/var/lib/lockdown/"..each)
  end
end
  

main()