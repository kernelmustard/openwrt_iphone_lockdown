include $(TOPDIR)/rules.mk

PKG_NAME:=lockdown
PKG_VERSION:=0.0.1
PKG_RELEASE:=1

PKG_SOURCE_PROTO:=git
PKG_SOURCE_URL:=https://gitlab.com/kernelmustard/openwrt_iphone_lockdown
PKG_SOURCE_VERSION:=master
PKG_SOURCE_SUBDIR:=$(PKG_NAME)

PKG_BUILD_DIR:=$(BUILD_DIR)/$(PKG_NAME)

include $(INCLUDE_DIR)/package.mk

define Build/Compile
	true
endef

define Package/$(PKG_NAME)
	SECTION:=net
	CATAGORY:=Network
	DEPENDS:=+usbmuxd +kmod-usb-core +kmod-usb-net +kmod-usb-net-ipheth +liblua
	TITLE:= IPHONE utility to lock the trust dialog and automaticly connect to openwrt.
	URL:=https://gitlab.com/kernelmustard/openwrt_iphone_lockdown
	PKGARCH:=all
endef

define Package/$(PKG_NAME)/description
IPHONE utility to lock the tust dialog and automaticly connect to opwnwrt on each boot.
endef

define Package/$(PKG_NAME)/install
	#Install lockdown to /usr/bin
	$(INSTALL_DIR) $(1) /usr/bin
	$(INSTALL_BIN)  ./lockdown $(1)/usr/bin
	$(INSTALL_DATA) ./lockdown.lua $(1)/usr/bin
	#Install lockdown init.d scripts
	$(INSTALL_DIR) $(1)/etc/init.d/
	$(INSTALL_BIN) ./etc/lockdown $(1)/etc/init.d/
	#Install usbmuxd init.d scripts
	$(INSTALL_DIR) $(1)/etc/init.d/
	$(INSTALL_BIN) ./etc/usbmuxd $(1)/etc/init.d/
	#Install locks directory
	$(INSTALL_DIR) $(1)/etc/lockdown/locks
	$(INSTALL_DATA) ./etc/LOCK_README /etc/lockdown/locks
endef



define Package/$(PKG_NAME)/postinst
#!/bin/sh
# check if we are on real system
if [ -z "$${IPKG_INSTROOT}" ]; then
        echo "Enabling rc.d symlink for usbmuxd"
        /etc/init.d/usbmuxd enable
fi

if [ -z "$${IPKG_INSTROOT}" ]; then
        echo "Enabling rc.d symlink for lockdown"
        /etc/init.d/lockdown enable
fi
exit 0
endef

define Package/$(PKG_NAME)/prerm
#!/bin/sh
# check if we are on real system
if [ -z "$${IPKG_INSTROOT}" ]; then
        echo "Removing rc.d symlink for usbmuxd"
        /etc/init.d/usbmuxd disable
fi
if [ -z "$${IPKG_INSTROOT}" ]; then
        echo "Removing rc.d symlink for lockdown"
        /etc/init.d/lockdown disable
fi
exit 0
endef



$(eval $(call BuildPackage,$(PKG_NAME)))